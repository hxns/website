import "whatwg-fetch";
// import "@babel/polyfill";

import "../css/main.scss";

window.play = function play(file) {
  const audio = document.getElementById(file);
  audio.play();
};

const twitchStreamListElement = document.querySelector(".twitch-streams");

const TWITCH_CLIENT_ID = "tc9cahsp64szossj09pfpvz1zuf4d1";
const HXNS_STREAMS = {
  34132474: "kerminaattori", //kermis
  23029178: "laznic", //laznic
  5652145: "redu__", //redu
  98811445: "ilihio", //ilihio
  38324005: "joodoosc2", //joodoosc2
  27358285: "hued", //hued
  141183392: "m1kroesa2252", //mikroesa,
  52290861: "snatchar",
};

Object.values(HXNS_STREAMS).forEach((name) => {
  const liElement = document.createElement("li");
  const linkElement = document.createElement("a");
  linkElement.setAttribute("href", `https://twitch.tv/${name}`);
  linkElement.setAttribute("class", name);
  linkElement.innerHTML = name;
  liElement.appendChild(linkElement);
  twitchStreamListElement.appendChild(liElement);
});

fetch(
  `https://api.twitch.tv/kraken/streams/?channel=${Object.keys(
    HXNS_STREAMS
  ).join(",")}`,
  {
    headers: {
      "Client-ID": TWITCH_CLIENT_ID,
      accept: "application/vnd.twitchtv.v5+json",
    },
  }
)
  .then((res) => res.json())
  .then((data) => {
    if (data.streams) {
      data.streams.forEach((stream) => {
        document
          .querySelector(`.${stream.channel.name}`)
          .classList.toggle("live");
      });
    }
  });
